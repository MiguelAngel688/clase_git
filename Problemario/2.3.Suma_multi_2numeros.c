#include <stdio.h>

int main(int argc, char const *argv[]) {
  int numero1=0;
  int numero2=0;
  int resultado1=0;
  int resultado2=0;
  printf("Dame un entero:\n");
  scanf("%d", &numero1);
  printf("Dame otro entero\n");
  scanf("%d", &numero2);
  resultado1 = numero1+numero2;
  resultado2 = numero1*numero2;
  printf("La suma de %d + %d es: %d\n", numero1, numero2, resultado1);
  printf("La multiplicacion de %d x %d es: %d\n", numero1, numero2, resultado2);
  return 0;
}
