#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]){
printf("------------------------------------------------------\n");
printf("-\n");
printf("-     Tipos de Datos y Direcciones de Memoria\n");
printf("-     Arroyo Zarco Miguel Angel\n");
printf("-     12/11/2018\n");
printf("-\n");
printf("------------------------------------------------------\n");

 char letra='a';
 unsigned char letraExtendida='a';

 short enteroCorto=10;
 int entero=30123;
 unsigned int enteroLargo=2345898898;

 float decimal=11.4f;
 double decimalDoblePRec=12.9989d;

 printf("Valor:%c direccion:%X, tamano:%d Byte(s)\n",letra,&letra,sizeof(letra));
 printf("Valor:%c direccion:%X, tamano:%d Byte(s)\n",letraExtendida,&letraExtendida,sizeof(letraExtendida));

 system("pause");
 return 0;

}

