﻿#include <stdio.h>

int main(int argc, char const *argv[]) {
  int var1=5;
  int var2=2;
  int resultado=0;

  //suma
  resultado=var1 + var2;
  printf("%d+%d= %d\n",var1,var2,resultado);
  //resta
  resultado=var1 - var2;
  printf("%d-%d= %d\n",var1,var2,resultado);
  //Multiplicación
  resultado=var1 * var2;
  printf("%d*%d= %d\n",var1,var2,resultado);
  //División
  resultado=var1 / var2;
  printf("%d / %d= %d\n",var1,var2,resultado);
  //Módulo
  resultado=var1 % var2;
  printf("%d mod %d= %d\n",var1,var2,resultado);

  return 0;
      

}
