#include <stdio.h>

int main(int argc, char const *argv[]) {
  int piezas=0;
  float precio=0.0f;
  float salario=0.0f;
  printf("¿Cuantas piezas se vendieron?\n");
  scanf("%d", &piezas);
  if (piezas<24) {
    precio=450; //Menudeo(menos de 24 piezas)
     if (piezas>=1 && piezas<=12) { //Salario del 5% del total de ventas
      salario=(piezas*precio)*0.05;
      printf("El salario es: $%.2f\n", salario);
    }
     if (piezas>=13 && piezas<=23) { //Salario del 7% del total de ventas con menudeo
      salario=(piezas*precio)*0.07;
      printf("El salario es: $%.2f\n", salario);
    }
  } else if (piezas>=24) {
    precio=435; //Mayoreo(mas de 24 piezas)
     if (piezas>=24 && piezas<=30) { //Salario del 7% del total de ventas con mayoreo
      salario=(piezas*precio)*0.07;
      printf("El salario es: $%.2f\n", salario);
    }
     if (piezas>30) { //Salario del 10% del total de ventas
      salario=(piezas*precio)*0.1;
      printf("El salario es: $%.2f\n", salario);
    }
  } else if (piezas<1) {
    printf("Valor invalido para calcular salario.\n");
  }
  return 0;
}
