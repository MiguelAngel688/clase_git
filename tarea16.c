#include <stdio.h>
#define MED 1
#define FIL 5
#define COL 3
int main(int argc, char const *argv[]) {
  int i=0;
  int j=0;

  for (i = 0; i < FIL; i++) {
    printf("+ ");
  }
  printf("\n");
  for (i = 0; i < COL; i++) {
    for (j = 0; j < MED; j++) {
      printf("+");
    }
    printf("       ");
    printf("+");
    printf("\n");
  }
  for (i = 0; i < FIL; i++) {
    printf("+ ");
  }
  printf("\n");
  return 0;
}
